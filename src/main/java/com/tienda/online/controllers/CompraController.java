package com.tienda.online.controllers;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tienda.online.exception.ModeloNotFoundException;
import com.tienda.online.models.Compra;
import com.tienda.online.services.CompraService;

@RestController
@RequestMapping("/api/compras")
public class CompraController {

	private CompraService compraService;

	public CompraController(CompraService compraService) {
		super();
		this.compraService = compraService;
	}

	@PostMapping
	public ResponseEntity<Compra> guardar(@RequestBody @Validated Compra compra) {
		Compra newCompra = compraService.guardar(compra);
		return ResponseEntity.status(HttpStatus.CREATED).body(newCompra);
	}

	@GetMapping
	public ResponseEntity<List<Compra>> listar() {
		List<Compra> lista = compraService.listar();
		return new ResponseEntity<>(lista, HttpStatus.OK);
	}

	@PutMapping
	public ResponseEntity<Compra> actualizar(@RequestBody @Validated Compra compra) {
		Compra updateCompra = compraService.actualizar(compra);
		return new ResponseEntity<Compra>(updateCompra, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Compra> obtenerPorId(@PathVariable(value="id") Integer id) {
		Compra compra = compraService.obtenerPorId(id);
		if(compra == null) {
			throw new ModeloNotFoundException("Compra con id: " + id + " no se encontro");
		}
//		return new ResponseEntity<Compra>(compra, HttpStatus.OK);
		return ResponseEntity.ok(compra);
	}

}










