package com.tienda.online.controllers;

import java.util.List;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tienda.online.exception.ModeloNotFoundException;
import com.tienda.online.models.Articulo;
import com.tienda.online.services.ArticuloService;

@RestController
@RequestMapping("/api/articulos")
public class ArticuloController {

	private ArticuloService articuloService;

	public ArticuloController(ArticuloService articuloService) {
		super();
		this.articuloService = articuloService;
	}

	@PostMapping
	public ResponseEntity<Articulo> guardar(@RequestBody @Validated Articulo articulo) {
		Articulo newArticulo = articuloService.guardar(articulo);
		if (newArticulo == null) {
			throw new DataIntegrityViolationException("Ya existe un articulo con nombre: " 
							+ articulo.getNombre());
		}
		return ResponseEntity.status(HttpStatus.CREATED).body(newArticulo);
	}

	@GetMapping
	public ResponseEntity<List<Articulo>> listar() {
		List<Articulo> lista = articuloService.listar();
		return new ResponseEntity<>(lista, HttpStatus.OK);
	}

	@PutMapping
	public ResponseEntity<Articulo> actualizar(@RequestBody @Validated Articulo articulo) {
		Articulo updateArticulo = articuloService.actualizar(articulo);
		return new ResponseEntity<Articulo>(updateArticulo, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Articulo> obtenerPorId(@PathVariable(value="id") Integer id) {
		Articulo articulo = articuloService.obtenerPorId(id);
		if(articulo == null) {
			throw new ModeloNotFoundException("Articulo con id: " + id + " no se encontro");
		}
//		return new ResponseEntity<Articulo>(articulo, HttpStatus.OK);
		return ResponseEntity.ok(articulo);
	}

}










