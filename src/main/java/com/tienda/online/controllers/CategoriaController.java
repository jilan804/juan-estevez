package com.tienda.online.controllers;

import java.util.List;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tienda.online.models.Categoria;
import com.tienda.online.services.CategoriaService;

@RestController
@RequestMapping("/api/categorias")
public class CategoriaController {

	private CategoriaService categoriaService;

	public CategoriaController(CategoriaService categoriaService) {
		super();
		this.categoriaService = categoriaService;
	}

	@PostMapping
	public ResponseEntity<Categoria> guardar(@RequestBody @Validated Categoria categoria) {
		Categoria newCategoria = categoriaService.guardar(categoria);
		if (newCategoria == null) {
			throw new DataIntegrityViolationException("Ya existe un categoria con id: " 
							+ categoria.getId());
		}
		return ResponseEntity.status(HttpStatus.CREATED).body(newCategoria);
	}

	@GetMapping
	public ResponseEntity<List<Categoria>> listar() {
		List<Categoria> lista = categoriaService.listar();
		return new ResponseEntity<>(lista, HttpStatus.OK);
	}

	@PutMapping
	public ResponseEntity<Categoria> actualizar(@RequestBody @Validated Categoria categoria) {
		Categoria updateCategoria = categoriaService.actualizar(categoria);
		return new ResponseEntity<Categoria>(updateCategoria, HttpStatus.OK);
	}

}










