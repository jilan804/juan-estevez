package com.tienda.online.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.tienda.online.models.Usuario;
import com.tienda.online.repositories.UsuarioRepository;

@Service("userDetailService")
public class UsuarioService implements UserDetailsService{

	private UsuarioRepository usuarioRepository;

	public UsuarioService(UsuarioRepository usuarioRepository) {
		this.usuarioRepository = usuarioRepository;
	}
	
	public Usuario guardar(Usuario usuario){
		usuario.setFecha(new Date());
		if(usuarioRepository.findByEmail(usuario.getEmail())!=null) {
			return null;
		}
		return usuarioRepository.save(usuario);
	}
	
	public List<Usuario> listar(){
		return (List<Usuario>) usuarioRepository.findAll();
	}
	
	public Usuario actualizar(Usuario usuario) {
		return usuarioRepository.save(usuario);
	}
	
	public void eliminar(Integer id) {
		usuarioRepository.deleteById(id);
	}
	public Usuario obtenerPorId(Integer id) {
		return usuarioRepository.findById(id).isPresent()?usuarioRepository.findById(id).get():null;
	}
	
	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		Usuario usuario = usuarioRepository.findOneByCorreoIgnoreCase(email).get();
		if (usuario == null) {
			throw new UsernameNotFoundException(email);
		}

		List<GrantedAuthority> authorities = new ArrayList<>();
		authorities.add(new SimpleGrantedAuthority(usuario.getRol().getNombre()));

		UserDetails userDetails = new User(usuario.getEmail(), usuario.getPassword(), authorities);
		return userDetails;
	}
}
