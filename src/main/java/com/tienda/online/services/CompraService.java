package com.tienda.online.services;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.tienda.online.models.Compra;
import com.tienda.online.models.Articulo;
import com.tienda.online.repositories.ArticuloRepository;
import com.tienda.online.repositories.CompraRepository;

@Service
public class CompraService {

	private CompraRepository compraRepository;
	private ArticuloRepository articuloRepository;
	
	public CompraService(CompraRepository compraRepository,ArticuloRepository articuloRepository) {
		this.compraRepository = compraRepository;
		this.articuloRepository = articuloRepository;
	}
	
	public Compra guardar(Compra compra){
		compra.setFecha(new Date());
		compra.setNumeroDocumento("000-000"+(compraRepository.count()+1));
		compra.getDetalleCompraList().forEach(detalle -> {
			Optional<Articulo> articulo = articuloRepository.findById(detalle.getArticulo().getId());
			if(articulo.isPresent()) {
				articulo.get().setCantidad(articulo.get().getCantidad() - detalle.getCantidad());
				articuloRepository.save(articulo.get());
				detalle.setCompra(compra);
			}
		});
		return compraRepository.save(compra);
	}
	
	public List<Compra> listar(){
		return (List<Compra>) compraRepository.findAll();
	}
	
	public Compra actualizar(Compra compra) {
		return compraRepository.save(compra);
	}
	
	public void eliminar(Compra compra) {
		compraRepository.delete(compra);
	}
}
