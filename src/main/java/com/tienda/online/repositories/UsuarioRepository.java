package com.tienda.online.repositories;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.tienda.online.models.Usuario;

public interface UsuarioRepository extends CrudRepository<Usuario, Integer>{
	
	Usuario findByEmail(String email);

	Optional<Usuario> findOneByCorreoIgnoreCase(String email);

}
