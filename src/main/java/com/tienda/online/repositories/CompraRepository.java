package com.tienda.online.repositories;

import org.springframework.data.repository.CrudRepository;

import com.tienda.online.models.Compra;

public interface CompraRepository extends CrudRepository<Compra, Long> {
	//Compra findById(Long id);
}
