package com.tienda.online.repositories;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.tienda.online.models.Articulo;
@Repository
public interface ArticuloRepository extends CrudRepository<Articulo, Long>{
	
	Articulo findByNombre(String nombre);
	
	@Query("SELECT a FROM Articulo a "
			+ "WHERE 	a.categoria.nombre 	=:categoriaNombre "
			+ "AND 		a.precio 			=:precio "
			+ "AND 		a.cantidad 			>=:cantidad")
	List<Articulo> reporteCliente(	@Param("categoriaNombre") String categoriaNombre
									,@Param("precio") BigDecimal precio
									,@Param("cantidad") Integer cantidad);
}
