package com.tienda.online.exception;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;


public class ExceptionResponse {
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd/MM/yyyy HH:mm:ss")
	private Date fecha;
	private String mensaje;
	private String detalle;
	
	public ExceptionResponse(Date fecha, String mensaje, String detalle) {
		super();
		this.fecha = fecha;
		this.mensaje = mensaje;
		this.detalle = detalle;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public String getDetalle() {
		return detalle;
	}
	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}
	
}
