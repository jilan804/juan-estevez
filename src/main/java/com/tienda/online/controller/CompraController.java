package com.tienda.online.controller;

import java.util.List;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.tienda.online.models.Compra;
import com.tienda.online.services.CompraService;

@RestController
@RequestMapping("/api/compra")
public class CompraController {
	private CompraService compraServices;

	public CompraController(CompraService compraServices) {
		super();
		this.compraServices = compraServices;
	}
	
	@PostMapping
	public ResponseEntity<Compra> guardar(@RequestBody @Validated Compra compra) {
		Compra newCompra = compraServices.guardar(compra);
		if(newCompra == null) {
			throw new DataIntegrityViolationException("Ya existe un compra con email: "+compra.getId());
		}
		return ResponseEntity.status(HttpStatus.CREATED).body(newCompra);
	}
	@GetMapping
	public ResponseEntity<List<Compra>> listar(){
		List<Compra> lista = compraServices.listar();
		return new ResponseEntity<>(lista,HttpStatus.OK);
	}
	@PutMapping
	public ResponseEntity<Compra> actualiza(@RequestBody Compra compra) {
		Compra updateCompra = compraServices.actualizar(compra);
		return new ResponseEntity<>(updateCompra,HttpStatus.OK);
	}

}
