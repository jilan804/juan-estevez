package com.tienda.online.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.token.TokenService;
import org.springframework.security.oauth2.provider.token.ConsumerTokenServices;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tienda.online.exception.ModelNotFoundException;
import com.tienda.online.models.Articulo;
import com.tienda.online.services.ArticuloService;

@RestController
@RequestMapping("/api/articulo")
public class ArticuloController {
	@Autowired
	private ArticuloService articuloServices;
	
	@Resource(name="tokenServices")
	private ConsumerTokenServices tokenService;
/*	public ArticuloController(ArticuloService articuloServices) {
		super();
		this.articuloServices = articuloServices;
	}
*/	
	@PostMapping
	public ResponseEntity<Articulo> guardar(@RequestBody @Validated Articulo articulo) {
		Articulo newArticulo = articuloServices.guardar(articulo);
		if(newArticulo == null) {
			throw new DataIntegrityViolationException("Ya existe un articulo con email: "+articulo.getId());
		}
		return ResponseEntity.status(HttpStatus.CREATED).body(newArticulo);
	}
	@GetMapping
	public ResponseEntity<List<Articulo>> listar(){
		List<Articulo> lista = articuloServices.listar();
		return new ResponseEntity<>(lista,HttpStatus.OK);
	}
	@PutMapping
	public ResponseEntity<Articulo> actualiza(@RequestBody Articulo articulo) {
		Articulo updateArticulo = articuloServices.actualizar(articulo);
		return new ResponseEntity<>(updateArticulo,HttpStatus.OK);
	}
	
	@GetMapping("/{nombre}")
	public ResponseEntity<Articulo> obtenerPorId(@PathVariable(value="nombre") String nombre){
		Articulo articulo = articuloServices.obtenerPorNombre(nombre);
		if(articulo==null) {
			throw new ModelNotFoundException("Articulo con id: "+nombre+" no se encontró");
		}
		//return new ResponseEntity<Articulo>(articulo,HttpStatus.OK);
		return ResponseEntity.ok(articulo);
	}
}
