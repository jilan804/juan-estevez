package com.tienda.online.controller;

import java.util.List;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tienda.online.exception.ModelNotFoundException;
import com.tienda.online.models.Usuario;
import com.tienda.online.services.UsuarioService;

@RestController
@RequestMapping("/api/usuario")
public class UsuarioController {
	private UsuarioService usuarioServices;

	public UsuarioController(UsuarioService usuarioServices) {
		super();
		this.usuarioServices = usuarioServices;
	}
	
	@PostMapping
	public ResponseEntity<Usuario> guardar(@RequestBody @Validated Usuario usuario) {
		Usuario newUsuario = usuarioServices.guardar(usuario);
		if(newUsuario == null) {
			throw new DataIntegrityViolationException("Ya existe un usuario con email: "+usuario.getEmail());
		}
		return ResponseEntity.status(HttpStatus.CREATED).body(newUsuario);
	}
	@GetMapping
	public ResponseEntity<List<Usuario>> listar(){
		List<Usuario> lista = usuarioServices.listar();
		return new ResponseEntity<>(lista,HttpStatus.OK);
	}
	@PutMapping
	public ResponseEntity<Usuario> actualiza(@RequestBody Usuario usuario) {
		Usuario updateUsuario = usuarioServices.actualizar(usuario);
		return new ResponseEntity<>(updateUsuario,HttpStatus.OK);
	}
	@GetMapping("/{id}")
	public ResponseEntity<Usuario> obtenerPorId(@PathVariable(value="id") Integer id){
		Usuario usuario = usuarioServices.obtenerPorId(id);
		if(usuario==null) {
			throw new ModelNotFoundException("Usuario con id: "+id+" no se encontró");
		}
		//return new ResponseEntity<Usuario>(usuario,HttpStatus.OK);
		return ResponseEntity.ok(usuario);
	}
	@DeleteMapping(path="/{id}")
	public void eliminar(@PathVariable(value="id") Integer id) {
		usuarioServices.eliminar(id);
	}
}
