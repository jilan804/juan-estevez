package com.tienda.online.controller;

import java.util.List;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.tienda.online.models.Categoria;
import com.tienda.online.services.CategoriaService;

@RestController
@RequestMapping("/api/categoria")
public class CategoriaController {
	private CategoriaService categoriaServices;

	public CategoriaController(CategoriaService categoriaServices) {
		super();
		this.categoriaServices = categoriaServices;
	}
	
	@PostMapping
	public ResponseEntity<Categoria> guardar(@RequestBody @Validated Categoria categoria) {
		Categoria newCategoria = categoriaServices.guardar(categoria);
		if(newCategoria == null) {
			throw new DataIntegrityViolationException("Ya existe un categoria con email: "+categoria.getId());
		}
		return ResponseEntity.status(HttpStatus.CREATED).body(newCategoria);
	}
	@GetMapping
	public ResponseEntity<List<Categoria>> listar(){
		List<Categoria> lista = categoriaServices.listar();
		return new ResponseEntity<>(lista,HttpStatus.OK);
	}
	@PutMapping
	public ResponseEntity<Categoria> actualiza(@RequestBody Categoria categoria) {
		Categoria updateCategoria = categoriaServices.actualizar(categoria);
		return new ResponseEntity<>(updateCategoria,HttpStatus.OK);
	}
	/*
	@GetMapping("/{id}")
	public ResponseEntity<Categoria> obtenerPorId(@PathVariable(value="id") Integer id){
		Categoria categoria = categoriaServices.listar()(id);
		if(categoria==null) {
			throw new ModelNotFoundException("Categoria con id: "+id+" no se encontró");
		}
		//return new ResponseEntity<Categoria>(categoria,HttpStatus.OK);
		return ResponseEntity.ok(categoria);
	}
	*/
}
